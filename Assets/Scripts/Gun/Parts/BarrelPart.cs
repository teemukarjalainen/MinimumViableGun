﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelPart : MonoBehaviour
{
    public GameObject barrelSpawner;

    public Vector3 GetBarrelSpawnPoint()
    {
        checkForNonExistentSpawner();

        return barrelSpawner.transform.position;
    }
    public Quaternion GetBarrelSpawnRotation()
    {
        checkForNonExistentSpawner();

        return Quaternion.Euler(barrelSpawner.transform.rotation.x, barrelSpawner.transform.rotation.y, barrelSpawner.transform.rotation.z + 90);
    }

    private void checkForNonExistentSpawner()
    {
        if (barrelSpawner == null)
        {
            barrelSpawner = GameObject.FindGameObjectWithTag(TagList.TAG_BARREL_SPAWNER);
        }
    }
}
