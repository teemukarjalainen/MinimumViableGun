﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceiverPart : MonoBehaviour
{
    public Transform barrelPos;

    private GameObject bullet;
    private BarrelPart barrelScript;

    public bool isAutomatic = false;
    internal bool hasFired = false;

    // Start is called before the first frame update
    void Start()
    {
        barrelScript = GameObject.FindGameObjectWithTag(TagList.TAG_BARREL).GetComponent<BarrelPart>();
    }

    public void TriggerDown()
    {
        if (!hasFired)
        {
            Fire();
        }
    }

    public void TriggerUp()
    {
        hasFired = false;
    }

    internal void Fire()
    {
        hasFired = true;
        if (bullet == null)
        {
            bullet = Resources.Load<GameObject>("Prefabs/BulletBase");
        }

        GameObject newBullet;
        newBullet = Instantiate(bullet, barrelScript.GetBarrelSpawnPoint(), barrelScript.GetBarrelSpawnRotation());
        newBullet.GetComponent<Rigidbody>().velocity = transform.TransformDirection(Vector3.left * 10);
    }

    internal void SetBullet(GameObject bullet)
    {
        this.bullet = bullet;
    }

    internal void SetBarrel(BarrelPart barrelScript)
    {
        this.barrelScript = barrelScript;
    }
}
