﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticReceiver : ReceiverPart
{
    public void Start()
    {
        this.isAutomatic = true;
    }
}
