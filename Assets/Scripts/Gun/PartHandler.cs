﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartHandler : MonoBehaviour
{
    private static string CLONE_SUFFIX = "(Clone)"; // Workaround because Unity is a piece of S*** when it instantiates a new object and forces the suffix

    public GameObject bullet;

    private GameObject barrel;
    private BarrelPart barrelScript;

    private GameObject savedBarrel;

    public GameObject receiver;
    private ReceiverPart receiverScript;

    // Start is called before the first frame update
    void Start()
    {
        receiver = Instantiate(GetReceiverWithName("DefaultReceiver"), transform); ;
        receiverScript = receiver.GetComponent<ReceiverPart>();

        barrel = Instantiate(GetBarrelWithName("DefaultBarrel"), receiverScript.barrelPos);
        barrelScript = barrel.GetComponent<BarrelPart>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SwitchBetweenReceivers();
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SwitchBetweenBarrels();
        }
    }

    private GameObject GetReceiverWithName(string receiverName)
    {
        GameObject receiver = Resources.Load<GameObject>("Prefabs/Gun/Mods/Receiver/" + receiverName);
        return receiver;
    }

    private GameObject GetBarrelWithName(string barrelName)
    {
        GameObject barrel = Resources.Load<GameObject>("Prefabs/Gun/Mods/Barrel/" + barrelName);
        return barrel;
    }

    private void SwitchBetweenReceivers()
    {
        string newReceiverName;

        // This is a testing method used to switch between default and automatic receiver
        if (receiver.name.Equals("DefaultReceiver" + CLONE_SUFFIX))
        {
            newReceiverName = "AutomaticReceiver";
        }
        else
        {
            newReceiverName = "DefaultReceiver";
        }

        SaveOtherParts();

        // Destroy old receiver
        GameObject.Destroy(receiver);
        // Instantiate new receiver
        receiver = Instantiate(GetReceiverWithName(newReceiverName), transform);
        // Replace old receiver script with new receiver script
        receiverScript = receiver.GetComponent<ReceiverPart>();

        ReinstantiateOtherParts();
    }

    private void SwitchBetweenBarrels()
    {
        string newBarrelName;

        // This is a testing method used to switch between default and long barrel
        if (barrel.name.Equals("DefaultBarrel" + CLONE_SUFFIX))
        {
            newBarrelName = "LongBarrel";
        }
        else
        {
            newBarrelName = "DefaultBarrel";
        }
        // Destroy old barrel
        GameObject.Destroy(barrel);
        // Instantiate new barrel
        barrel = Instantiate(GetBarrelWithName(newBarrelName), receiverScript.barrelPos);
        // Replace old barrel script with new barrel script
        barrelScript = barrel.GetComponent<BarrelPart>();
    }

    private void SaveOtherParts()
    {
        savedBarrel = barrel;
    }

    private void ReinstantiateOtherParts()
    {
        barrel = Instantiate(savedBarrel, receiverScript.barrelPos);
        barrelScript = barrel.GetComponent<BarrelPart>();
        receiverScript.SetBarrel(barrelScript);

        //Remember to give the new receiver to player controls for firing purposes
        GameObject.FindGameObjectWithTag(TagList.TAG_PLAYER).GetComponent<PlayerControls>().SetReceiver(receiver.GetComponent<ReceiverPart>());

        // Don't forget the bullet!
        receiverScript.SetBullet(bullet);
    }

    public GameObject GetBarrel()
    {
        return barrel;
    }

    public void SetBarrel(GameObject barrel)
    {
        this.barrel = barrel;
    }
}
