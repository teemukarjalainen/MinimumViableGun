﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TagList
{
    public static string TAG_PLAYER = "Player";

    public static string TAG_BARREL = "Barrel";
    public static string TAG_BARREL_SPAWNER = "BarrelSpawner";

    public static string TAG_RECEIVER = "Receiver";
}
