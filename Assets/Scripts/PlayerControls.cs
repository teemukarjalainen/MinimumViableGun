﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{

    public ReceiverPart receiver;

    // Start is called before the first frame update
    void Start()
    {
        receiver = GameObject.FindGameObjectWithTag(TagList.TAG_RECEIVER).GetComponent<ReceiverPart>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            receiver.TriggerDown();
        }
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            receiver.TriggerUp();
        }
    }

    internal void SetReceiver(ReceiverPart receiver)
    {
        this.receiver = receiver;
    }
}
